package com.house.car_maintainence;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.house.car_maintainence.model.carInventory;
import com.house.car_maintainence.repository.CarInventoryConfigRepository;
import com.house.car_maintainence.service.CarInventoryService;
import org.junit.Before;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@ActiveProfiles("local")
@WebMvcTest(carInventory.class)
class CarMaintainenceApplicationTests {

    @MockBean
    private CarInventoryConfigRepository carSelectionConfigRepository;

    @MockBean
    CarInventoryService carSelectionService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    @DisplayName("Creating new car Entry")
    public void createCarEntryTest() throws Exception {
//        CarSelection carSelection = new CarSelection();
//        Mockito.when(carSelectionService.createNewCarEntry(any())).thenReturn(CarSelection.builder().build());
//        MvcResult result = mockMvc.perform(post("/v0/carSelection")
//                .content(mapper.writeValueAsString(CarSelection.builder().build()))
//                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
    }

}
