package com.house.car_maintainence.utilities;

import com.house.car_maintainence.model.issueBlob;
import com.house.car_maintainence.model.request.ServiceReqRequest;
import com.house.car_maintainence.model.request.issueBlobRequest;
import org.apache.commons.lang3.RandomStringUtils;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public final class CommonUtils {

    public static boolean isValidCarId(String carId) {
        return carId.matches("AVI_RESI-[A-Z0-9]{4,}$");
    }

    public static String generateCarId() {
        return "AVI_RESI-".concat(RandomStringUtils.randomAlphanumeric(5)).toUpperCase(Locale.ROOT);
    }

    public static OffsetDateTime convertToOffsetDate(String Date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy",Locale.ENGLISH);
        LocalDate localDate = LocalDate.parse(Date, formatter);
        ZonedDateTime dateTime = localDate.atStartOfDay(ZoneId.systemDefault());
        return dateTime.toOffsetDateTime();
    }

    public static String generateServiceReqId() {
        return "SREQ-".concat(RandomStringUtils.randomAlphanumeric(5)).toUpperCase(Locale.ROOT);
    }

    public static String generateIssueId() {
        return "ISS-".concat(RandomStringUtils.randomAlphanumeric(5)).toUpperCase(Locale.ROOT);
    }

    public static List<issueBlob> processIssues(ServiceReqRequest ServiceReqRequest) {
        List<issueBlob> result = new ArrayList<>();
        if(ServiceReqRequest !=null && ServiceReqRequest.getIssue().size() > 0) {
            for(issueBlobRequest issue : ServiceReqRequest.getIssue() ) {
                issueBlob issueItem = issueBlob.builder()
                        .issueId(generateIssueId())
                        .issueType(issue.getIssueType())
                        .issueDate(issue.getIssueDate())
                        .issueDesc(issue.getIssueDesc())
                        .isUrgent(issue.isUrgent())
                        .currentStatus(issue.getCurrentStatus())
                        .kmsRun(ServiceReqRequest.getKmsRun())
                        .createdAt("Anand Palaniappan Annamalai")
                        .createdAt(OffsetDateTime.now().toString())
                        .build();
                result.add(issueItem);
            }
        }
        return result;
    }

    public static String generateServiceSeqId() {
        return "HIST-".concat(RandomStringUtils.randomAlphanumeric(5)).toUpperCase(Locale.ROOT);
    }
}
