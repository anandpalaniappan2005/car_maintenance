package com.house.car_maintainence.controller;

import com.house.car_maintainence.exceptions.ServiceRequestNotFoundException;
import com.house.car_maintainence.model.carPendingReq;
import com.house.car_maintainence.model.carServiceReq;
import com.house.car_maintainence.service.CarPendingReqService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Car Pending Request Controller")
@RestController
@RequestMapping("v0")
public class carPendingRequestController {

    private final CarPendingReqService carPendingReqService;

    @Autowired
    public carPendingRequestController(CarPendingReqService carPendingReqService) {
        this.carPendingReqService = carPendingReqService;
    }

    @Operation(summary = "Transform an Service Request to a new Pending Request and deletes it from Service Request Table")
    @PostMapping("/carPendingServiceRequest")
    public ResponseEntity<carPendingReq> createOrAddNewPendingRequest(@RequestBody carServiceReq carServiceRequest) {
        return ResponseEntity.ok().body(carPendingReqService.createOrAddNewPendingReqEntry(carServiceRequest));
    }

    @Operation(summary = "Fetches the Pending Service Request by Service RequestId")
    @GetMapping("/carPendingServiceRequest/{carId}")
    public ResponseEntity<carPendingReq> getPendingServiceRequestBySReqId(@PathVariable("carId") String id ) throws ServiceRequestNotFoundException {
        return ResponseEntity.ok().body(carPendingReqService.fetchDetailsById(id));
    }
}
