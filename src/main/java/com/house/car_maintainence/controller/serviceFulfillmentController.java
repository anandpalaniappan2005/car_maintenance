package com.house.car_maintainence.controller;

import com.house.car_maintainence.exceptions.FulfilmentNotFoundException;
import com.house.car_maintainence.model.carServiceReq;
import com.house.car_maintainence.model.fulfilment;
import com.house.car_maintainence.model.issueBlob;
import com.house.car_maintainence.service.fulfilmentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "Car's Service Request Fulfillment Controller")
@RestController
@RequestMapping("v0")
public class serviceFulfillmentController {


    private final fulfilmentService fulfilmentService;


    public serviceFulfillmentController(com.house.car_maintainence.service.fulfilmentService fulfilmentService) {
        this.fulfilmentService = fulfilmentService;
    }

    @Operation(summary = "Fetches the exact record in Service History Table")
    @GetMapping("/fulfilment/{fulfilmentId}")
    public ResponseEntity<fulfilment> getFulfilmentRecordById(@PathVariable("fulfilmentId") String id) throws FulfilmentNotFoundException {
        return ResponseEntity.ok().body(fulfilmentService.fetchFulfilmentById(id));
    }

    @Operation(summary = "Creates an Entry on to the Service History Table")
    @PostMapping("/fulfilment")
    public ResponseEntity<fulfilment> createNewFulfilmentServiceEntry(
            @RequestParam String carId,
            @RequestParam double kmsRun,
            @RequestBody List<issueBlob> issueList,
            @RequestParam String comments,
            @RequestParam double totalCost
    ) {
        return ResponseEntity.ok().body(fulfilmentService.processFulfilments(carId, kmsRun, issueList, comments,
                totalCost));
    }
}
