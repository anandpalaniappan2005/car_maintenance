package com.house.car_maintainence.controller;

import com.house.car_maintainence.exceptions.ServiceRequestNotFoundException;
import com.house.car_maintainence.model.carServiceReq;
import com.house.car_maintainence.model.request.ServiceReqRequest;
import com.house.car_maintainence.service.CarServiceReqService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Car Service Request Controller")
@RestController
@RequestMapping("v0")
public class carServiceRequestController {

    private final CarServiceReqService carServiceReqService;

    @Autowired
    public carServiceRequestController(CarServiceReqService carServiceReqService) {
        this.carServiceReqService = carServiceReqService;
    }

    @Operation(summary = "Creates a new Service Entry")
    @PostMapping("/carServiceRequest")
    public ResponseEntity<carServiceReq> createNewServiceRequest(@RequestBody ServiceReqRequest ServiceReqRequest) {
        return ResponseEntity.ok().body(carServiceReqService.createNewServiceRequestEntry(ServiceReqRequest));
    }

    @Operation(summary = "Fetches the Service Request by Service RequestId")
    @GetMapping("/carServiceRequest/{carServiceRequestId}")
    public ResponseEntity<carServiceReq> getServiceRequestBySReqId(@PathVariable ("carServiceRequestId") String id ) throws ServiceRequestNotFoundException {
        return ResponseEntity.ok().body(carServiceReqService.fetchDetailsBySReqId(id));
    }

}
