package com.house.car_maintainence.controller;

import com.house.car_maintainence.exceptions.CarInventoryException;
import com.house.car_maintainence.model.carInventory;
import com.house.car_maintainence.model.request.carInventoryRequest;
import com.house.car_maintainence.service.CarInventoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Car Inventory Controller")
@RestController
@RequestMapping("v0")
public class carInventoryController {
    private final CarInventoryService carInventoryService;

    @Autowired
    public carInventoryController(CarInventoryService carInventoryService) {
        this.carInventoryService = carInventoryService;
    }

    @Operation(summary = "Create a new Car Entry")
    @PostMapping("/carInventory")
    public ResponseEntity<carInventory> createCarInventory(@RequestBody carInventoryRequest carInventory) {
        return ResponseEntity.ok().body(carInventoryService.createNewCarEntry(carInventory));
    }

    @Operation(summary = "Fetch Car Data for CarSelectionId")
    @GetMapping("/carInventory/{carInventoryId}")
    public ResponseEntity<carInventory> getCarInventory(@PathVariable("carInventoryId") String id) throws CarInventoryException {
        return ResponseEntity.ok().body(carInventoryService.fetchCarById(id));
    }
}
