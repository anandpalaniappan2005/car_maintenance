package com.house.car_maintainence.exceptions;

import com.house.car_maintainence.exceptions.model.ExceptionResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = {Exception.class})
    protected ExceptionResponse internalError(Exception ec) {
        return ExceptionResponse.builder().errorMessage(ec.getMessage()).build();
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = {CarNotFoundException.class})
    protected ExceptionResponse carNotFoundSelection(Exception ex) {
        return ExceptionResponse.builder().errorMessage(ex.getMessage()).build();
    }
}
