package com.house.car_maintainence.exceptions;

public class CarNotFoundException extends CarInventoryException {
    public CarNotFoundException(String errorMessage) {
        super(errorMessage);
    }

    public CarNotFoundException(String errorMessage, Exception e) {
        super(errorMessage, e);
    }
}
