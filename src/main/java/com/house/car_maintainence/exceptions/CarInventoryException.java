package com.house.car_maintainence.exceptions;

public class CarInventoryException extends Exception{

    public CarInventoryException(String errorMessage) {
        this(errorMessage, (Exception)null);
    }
    public CarInventoryException(String errorMessage, Exception e) {
        super(errorMessage,e);
    }
}
