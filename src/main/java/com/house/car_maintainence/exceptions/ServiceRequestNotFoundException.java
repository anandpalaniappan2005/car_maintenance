package com.house.car_maintainence.exceptions;

public class ServiceRequestNotFoundException extends CarNotFoundException{
    public ServiceRequestNotFoundException(String errorMessage) {
        super(errorMessage);
    }

    public ServiceRequestNotFoundException(String errorMessage, Exception e) {
        super(errorMessage, e);
    }
}
