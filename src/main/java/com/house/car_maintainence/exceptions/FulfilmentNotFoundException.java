package com.house.car_maintainence.exceptions;

public class FulfilmentNotFoundException extends CarNotFoundException{
    public FulfilmentNotFoundException(String errorMessage) {
        super(errorMessage);
    }

    public FulfilmentNotFoundException(String errorMessage, Exception e) {
        super(errorMessage, e);
    }
}