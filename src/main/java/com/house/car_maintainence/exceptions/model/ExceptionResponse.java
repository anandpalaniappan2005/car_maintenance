package com.house.car_maintainence.exceptions.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ExceptionResponse {
    private String errorMessage;
}
