package com.house.car_maintainence.repository;

import com.house.car_maintainence.model.carServiceReq;
import org.socialsignin.spring.data.dynamodb.repository.DynamoDBCrudRepository;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;

@EnableScan
public interface CarServiceRequestConfigRepository extends DynamoDBCrudRepository<carServiceReq, String> {
}
