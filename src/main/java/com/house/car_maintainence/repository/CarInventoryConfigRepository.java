package com.house.car_maintainence.repository;

import com.house.car_maintainence.model.carInventory;
import org.socialsignin.spring.data.dynamodb.repository.DynamoDBCrudRepository;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;

@EnableScan
public interface CarInventoryConfigRepository extends DynamoDBCrudRepository<carInventory, String> {
}
