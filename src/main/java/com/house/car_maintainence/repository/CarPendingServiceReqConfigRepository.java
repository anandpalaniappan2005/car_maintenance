package com.house.car_maintainence.repository;

import com.house.car_maintainence.model.carPendingReq;
import org.socialsignin.spring.data.dynamodb.repository.DynamoDBCrudRepository;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;

@EnableScan
public interface CarPendingServiceReqConfigRepository extends DynamoDBCrudRepository<carPendingReq,String> {
}
