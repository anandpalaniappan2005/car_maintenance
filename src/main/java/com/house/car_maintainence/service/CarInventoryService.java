package com.house.car_maintainence.service;


import com.house.car_maintainence.exceptions.CarInventoryException;
import com.house.car_maintainence.exceptions.CarNotFoundException;
import com.house.car_maintainence.model.carInventory;
import com.house.car_maintainence.model.request.carInventoryRequest;
import com.house.car_maintainence.repository.CarInventoryConfigRepository;
import com.house.car_maintainence.utilities.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CarInventoryService {

    private final CarInventoryConfigRepository carInventoryConfigRepository;

    @Autowired
    public CarInventoryService(CarInventoryConfigRepository carInventoryConfigRepository) {
        this.carInventoryConfigRepository = carInventoryConfigRepository;
    }

    public carInventory fetchCarById(String id) throws CarInventoryException {
        Optional<carInventory> optionalCarInventory = carInventoryConfigRepository.findById(id);
        if (optionalCarInventory.isPresent()) {
            try {
                return optionalCarInventory.get();
            } catch (Exception e) {
                throw new CarInventoryException(e.getMessage());
            }
        } else {
            throw new CarNotFoundException("Car Id" + id + " is not found");
        }
    }

    public carInventory createNewCarEntry(carInventoryRequest carInventoryItem) {
        carInventory result = carInventory.builder()
                .carId(CommonUtils.generateCarId())
                .carName(carInventoryItem.getCarName())
                .carColour(carInventoryItem.getCarColour())
                .carCompany(carInventoryItem.getCarCompany())
                .numberPlate(carInventoryItem.getNumberPlate())
                .fuelType(carInventoryItem.getFuelType())
                .purchaseDate(CommonUtils.convertToOffsetDate(carInventoryItem.getPurchaseDate()).toString())
                .insuranceName(carInventoryItem.getInsuranceName())
                .insuranceStartDate(CommonUtils.convertToOffsetDate(carInventoryItem.getInsuranceStartDate()).toString())
                .insuranceEndDate(CommonUtils.convertToOffsetDate(carInventoryItem.getInsuranceEndDate()).toString())
                .build();
        carInventoryConfigRepository.save(result);
        return result;
    }
}
