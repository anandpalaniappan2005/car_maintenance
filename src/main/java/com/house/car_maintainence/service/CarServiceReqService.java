package com.house.car_maintainence.service;

import com.house.car_maintainence.exceptions.ServiceRequestNotFoundException;
import com.house.car_maintainence.model.carServiceReq;
import com.house.car_maintainence.model.issueBlob;
import com.house.car_maintainence.model.request.ServiceReqRequest;
import com.house.car_maintainence.repository.CarPendingServiceReqConfigRepository;
import com.house.car_maintainence.repository.CarServiceRequestConfigRepository;
import com.house.car_maintainence.utilities.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

import static com.house.car_maintainence.utilities.CommonUtils.processIssues;

@Service
public class CarServiceReqService {

    private final CarServiceRequestConfigRepository carServiceRequestConfigRepository;
    private final CarPendingReqService carPendingReqService;

    @Autowired
    public CarServiceReqService(CarServiceRequestConfigRepository serviceRequestConfigRepository, CarPendingServiceReqConfigRepository carPendingServiceReqConfigRepository, CarPendingReqService carPendingReqService) {
        this.carServiceRequestConfigRepository = serviceRequestConfigRepository;
        this.carPendingReqService = carPendingReqService;
    }

   public carServiceReq fetchDetailsBySReqId(String id) throws ServiceRequestNotFoundException {
       Optional<carServiceReq> optionalCarServiceReq = carServiceRequestConfigRepository.findById(id);
       if(optionalCarServiceReq.isPresent()) {
           return optionalCarServiceReq.get();
       }
       else {
           throw new ServiceRequestNotFoundException("There is no Service Request for this id");
       }
   }

   public carServiceReq createNewServiceRequestEntry(ServiceReqRequest serviceReqRequestItem) {
        List<issueBlob> issueBlobList = processIssues(serviceReqRequestItem);
        carServiceReq result = carServiceReq.builder()
                .carId(serviceReqRequestItem.getCarId())
                .serviceRequestId(CommonUtils.generateServiceReqId())
                .issue(issueBlobList)
                .createdAt(OffsetDateTime.now().toString())
                .kmsRun(serviceReqRequestItem.getKmsRun())
                .createdBy("Anand Palaniappan Annamalai")
                .overAllStatus("NOT Started")
                .build();

        carPendingReqService.createOrAddNewPendingReqEntry(result);
        carServiceRequestConfigRepository.save(result);
        return result;

   }
}
