package com.house.car_maintainence.service;

import com.house.car_maintainence.exceptions.FulfilmentNotFoundException;
import com.house.car_maintainence.model.fulfilment;
import com.house.car_maintainence.model.issueBlob;
import com.house.car_maintainence.repository.ServiceFulfilmentConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

import static com.house.car_maintainence.utilities.CommonUtils.generateServiceSeqId;

@Service
public class fulfilmentService {

    private final ServiceFulfilmentConfigRepository serviceFulfilmentConfigRepository;

    @Autowired
    public fulfilmentService(ServiceFulfilmentConfigRepository serviceFulfilmentConfigRepository) {
        this.serviceFulfilmentConfigRepository = serviceFulfilmentConfigRepository;
    }

    public fulfilment fetchFulfilmentById(String id) throws FulfilmentNotFoundException {
        Optional<fulfilment> fulfilmentOptional = serviceFulfilmentConfigRepository.findById(id);
        if (fulfilmentOptional.isPresent()) {
            return fulfilmentOptional.get();
        }
        throw new FulfilmentNotFoundException("This is no FulFilment with that id");
    }

    public fulfilment processFulfilments(String carId, double kmsRun, List<issueBlob> issueList,
                                         String comments, double totalCost
    ) {

        fulfilment result = fulfilment.builder()
                .carId(carId)
                .issue(issueList)
                .serviceSeqId(generateServiceSeqId())
                .totalCost(totalCost)
                .createdBy("Anand Palaniappan")
                .createdAt(OffsetDateTime.now().toString())
                .comments(comments)
                .kmsRun(kmsRun)
                .build();
        serviceFulfilmentConfigRepository.save(result);
        return result;

    }
}