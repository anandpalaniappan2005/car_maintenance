package com.house.car_maintainence.service;

import com.house.car_maintainence.exceptions.ServiceRequestNotFoundException;
import com.house.car_maintainence.model.carPendingReq;
import com.house.car_maintainence.model.carServiceReq;
import com.house.car_maintainence.model.issueBlob;
import com.house.car_maintainence.repository.CarPendingServiceReqConfigRepository;
import com.house.car_maintainence.repository.CarServiceRequestConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CarPendingReqService {

    private final CarPendingServiceReqConfigRepository carPendingServiceReqConfigRepository;

    @Autowired
    public CarPendingReqService(CarPendingServiceReqConfigRepository carPendingServiceReqConfigRepository, CarServiceRequestConfigRepository carServiceRequestConfigRepository) {
        this.carPendingServiceReqConfigRepository = carPendingServiceReqConfigRepository;
    }

    public carPendingReq fetchDetailsById(String id) throws ServiceRequestNotFoundException {
        Optional<carPendingReq> optionalCarPendingReq = carPendingServiceReqConfigRepository.findById(id);
        if (optionalCarPendingReq.isPresent()) {
            return optionalCarPendingReq.get();
        }
        throw new ServiceRequestNotFoundException("There is no Pending Request with the given id");
    }

    public carPendingReq createOrAddNewPendingReqEntry(carServiceReq carServiceReq) {
        Optional<carPendingReq> carPendingReqOptional =
                carPendingServiceReqConfigRepository.findById(carServiceReq.getCarId());

        if(carPendingReqOptional.isPresent()) {
            carPendingReq sourceCarPendingReq = carPendingReqOptional.get();
            List<issueBlob> issueSourceList = sourceCarPendingReq.getIssue();
            issueSourceList.addAll(carServiceReq.getIssue());
            carPendingServiceReqConfigRepository.save(sourceCarPendingReq);
            return sourceCarPendingReq;
        }
        System.out.println("It doesn't have the car ID");
        carPendingReq result = carPendingReq.builder()
                .carId(carServiceReq.getCarId())
                .issue(carServiceReq.getIssue())
                .build();
        carPendingServiceReqConfigRepository.save(result);
        return result;
    }
}
