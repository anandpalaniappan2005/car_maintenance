package com.house.car_maintainence.config.swagger;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Collections;

@Configuration
public class OpenAPIConfig {

    @Profile("local")
    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI().servers(Collections.singletonList(new Server().url("/").description("/")))
                .info(new Info().title("Car Maintenance Service").version("1.0")
                        .contact(new Contact().email("anandpalaniappan2005@gmail.com").name(": Anand Palaniappan Annamalai")));
    }

    @Bean
    public GroupedOpenApi v0_Apis() {
        return GroupedOpenApi.builder()
                .pathsToMatch("/v0/**")
                .group("v0")
                .build();
    }
}
