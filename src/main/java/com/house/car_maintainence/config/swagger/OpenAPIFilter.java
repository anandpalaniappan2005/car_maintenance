package com.house.car_maintainence.config.swagger;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(0)
public class OpenAPIFilter implements Filter {

    public static String headers = "Content-Type, authorization, cache-control";
    public static String methods = "GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE";

    @Override
    public void doFilter(
            ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        if (req.getServletPath().matches("^.*/swagger/v[0-9]$")) {
            String origin = req.getHeader(HttpHeaders.ORIGIN);
            resp.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, origin);
            resp.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, headers);
            resp.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS, methods);
            resp.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
            request.getRequestDispatcher(((HttpServletRequest) request).getServletPath()).forward(req, resp);
        } else {
            chain.doFilter(request, response);
        }
    }

}
