package com.house.car_maintainence;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
// @OpenAPIDefinition(info = @Info(title = "Car Maintenance Service", version = "1.0", description = "Maintains the KAP Cars"))
public class CarMaintainenceApplication {

    public static void main(String[] args) {

        SpringApplicationBuilder springApplicationBuilder = null;
        String env = System.getenv().getOrDefault("ENV","local");
        springApplicationBuilder = new SpringApplicationBuilder(CarMaintainenceApplication.class).profiles(env);
        springApplicationBuilder.run(args);
        //SpringApplication.run(CarMaintainenceApplication.class, args);
    }

}
