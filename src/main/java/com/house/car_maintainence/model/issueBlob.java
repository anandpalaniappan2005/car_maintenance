package com.house.car_maintainence.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@DynamoDBDocument
public class issueBlob {

    private String issueId;
    private String issueType;
    private String issueDesc;
    private String issueDate;
    private String currentStatus;
    private boolean isUrgent;

    // Used only for Pending Request's Table
    private double kmsRun;
    private String createdBy;
    private String createdAt;

}
