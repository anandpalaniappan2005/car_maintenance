package com.house.car_maintainence.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@DynamoDBTable(tableName = "serviceHistory")
public class fulfilment {

    @DynamoDBAttribute
    private String carId;

    @DynamoDBAttribute
    private double kmsRun;

    @DynamoDBAttribute
    private String createdBy;

    @DynamoDBAttribute
    private String createdAt;

    @DynamoDBAttribute
    private List<issueBlob> issue;

    @DynamoDBAttribute
    private String comments;

    @DynamoDBAttribute
    private double totalCost;

    @DynamoDBHashKey(attributeName = "serviceSeqId")
    private String serviceSeqId;
}
