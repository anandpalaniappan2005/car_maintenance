package com.house.car_maintainence.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConvertedEnum;
import com.house.car_maintainence.constants.FuelType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@DynamoDBTable(tableName = "carInventory")
public class carInventory {
    @DynamoDBHashKey(attributeName = "id")
    private String carId;

    @DynamoDBAttribute
    private String carName;

    @DynamoDBAttribute
    private String carCompany;

    @DynamoDBAttribute
    private String carColour;

    @DynamoDBAttribute
    private String numberPlate;

    @DynamoDBAttribute
    private String purchaseDate;

    private FuelType fuelType;

    @DynamoDBTypeConvertedEnum
    @DynamoDBAttribute(attributeName = "fuelType")
    public FuelType getFuelType() {
        return fuelType;
    }

    @DynamoDBAttribute
    private String insuranceName;

    @DynamoDBAttribute
    private String insuranceStartDate;

    @DynamoDBAttribute
    private String insuranceEndDate;
}
