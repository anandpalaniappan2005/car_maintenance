package com.house.car_maintainence.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@DynamoDBTable(tableName = "carServiceRequest")
public class carServiceReq {

    @DynamoDBAttribute
    private String carId;

    @DynamoDBHashKey(attributeName = "id")
    private String serviceRequestId;

    @DynamoDBAttribute
    private String overAllStatus;

    @DynamoDBAttribute
    private double kmsRun;

    @DynamoDBAttribute
    private String createdBy;

    @DynamoDBAttribute
    private String createdAt;

    @DynamoDBAttribute
    private List<issueBlob> issue;
}
