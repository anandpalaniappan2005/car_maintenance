package com.house.car_maintainence.model.request;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ServiceReqRequest {

    private String carId;
    private double kmsRun;
    private List<issueBlobRequest> issue;
}
