package com.house.car_maintainence.model.request;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.house.car_maintainence.constants.FuelType;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Data
@Builder
@Getter
@Setter
public class carInventoryRequest {

    private String carName;
    private String carColour;
    private String carCompany;
    private String numberPlate;
    private String purchaseDate;
    private FuelType fuelType;
    private String insuranceName;
    private String insuranceStartDate;
    private String insuranceEndDate;
}
