package com.house.car_maintainence.model.request;

import com.house.car_maintainence.model.issueBlob;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class carPendingServiceRequest {

    private String carId;
    // private String overAllStatus;
    private List<issueBlob> issue;
    private String createdBy;
}
