package com.house.car_maintainence.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class issueBlobRequest {

    private String issueType;
    private String issueDesc;
    private String issueDate;
    private String currentStatus;
    private boolean isUrgent;
}
