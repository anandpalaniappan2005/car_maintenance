package com.house.car_maintainence.constants;

public enum FuelType {
    PETROL,
    DIESEL,
    ELECTRIC
}